<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeFeatureNested extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:nested {FeatureName} {FeatureNames} {feature_name} {feature_names} {columns}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new nested feature in CRUD and REST API';

    protected function getStub($type)
    {
        return file_get_contents("stubs/$type.stub");
    }

    protected function apiController($name)
    {
        $apiControllerTemplate = str_replace(
            [
                '{{ class }}',
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}',
                '{{ namespace }}',
                '{{ namespacedModel }}',
                '{{ rootNamespace }}',
            ],
            [
                $name . "APIController",
                $name,
                strtolower($name),
                strtolower($names),
                'App\Http\Controllers\API',
                'App\\' . $name,
                'App\\'
            ],
            $this->getStub('controller.api')
        );

        file_put_contents(app_path("/Http/Controllers/API/{$name}APIController.php"), $apiControllerTemplate);
    }

    protected function blade($name)
    {
        if(!file_exists($path = resource_path("/views/{strtolower($names)}")))
            mkdir($path, 0777, true);
    }
    
    protected function blade_create($name)
    {
        $bladeCreateTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($names)
            ],
            $this->getStub('blade.create')
        );

        file_put_contents(resource_path("/views/{strtolower($names)}/create.blade.php"), $bladeCreateTemplate);
    }

    protected function blade_edit($name)
    {
        $bladeEditTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($names)
            ],
            $this->getStub('blade.edit')
        );

        file_put_contents(resource_path("/views/{strtolower($names)}/edit.blade.php"), $bladeEditTemplate);
    }

    protected function blade_index($name)
    {
        $bladeIndexTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($names)
            ],
            $this->getStub('blade.index')
        );

        file_put_contents(resource_path("/views/{strtolower($names)}/index.blade.php"), $bladeIndexTemplate);
    }

    protected function blade_show($name)
    {
        $bladeShowTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($names)
            ],
            $this->getStub('blade.show')
        );

        file_put_contents(resource_path("/views/{strtolower($names)}/show.blade.php"), $bladeShowTemplate);
    }

    protected function controller($name)
    {
        $controllerTemplate = str_replace(
            [
                '{{ class }}',
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}',
                '{{ namespace }}',
                '{{ namespacedModel }}',
                '{{ rootNamespace }}',
            ],
            [
                $name . "Controller",
                $name,
                strtolower($name),
                strtolower($names),
                'App\Http\Controllers',
                'App\\' . $name,
                'App\\'
            ],
            $this->getStub('controller')
        );

        file_put_contents(app_path("/Http/Controllers/{$name}Controller.php"), $controllerTemplate);
    }

    protected function migration($name)
    {
        $migrationTemplate = str_replace(
            [
                '{{ class }}',
                '{{ table }}'
            ],
            [
                'Create' . $names . 'Table',
                strtolower($names)
            ],
            $this->getStub('migration.create')
        );

        file_put_contents(database_path("/migrations/2020_04_12_0000000_create_{strtolower($names)}_table.php"), $migrationTemplate);
    }

    protected function model($name)
    {
        $modelTemplate = str_replace(
            [
                '{{ class }}',
                '{{ namespace }}',
                '{{ table }}'
            ],
            [
                $name,
                'App\\',
                strtolower($names)
            ],
            $this->getStub('model')
        );

        file_put_contents(app_path("/{$name}.php"), $modelTemplate);
    }

    protected function request($name)
    {
        $requestTemplate = str_replace(
            [
                '{{ class }}',
                '{{ model }}',
                '{{ modelVariable }}'
            ],
            [
                $name . "Request",
                $name,
                strtolower($name)
            ],
            $this->getStub('request')
        );

        if(!file_exists($path = app_path('/Http/Requests')))
            mkdir($path, 0777, true);

        file_put_contents(app_path("/Http/Requests/{$name}Request.php"), $requestTemplate);
    }

    protected function resource($name)
    {
        $resourceTemplate = str_replace(
            [
                '{{ class }}',
                '{{ namespace }}'
            ],
            [
                $name . "Resource",
                'App\Http\Resources\\',
            ],
            $this->getStub('resource')
        );

        if(!file_exists($path = app_path('/Http/Resources')))
            mkdir($path, 0777, true);

        file_put_contents(app_path("/Http/Resources/{$name}Resource.php"), $resourceTemplate);
    }

    protected function resource_collection($name)
    {
        $resourceCollectionTemplate = str_replace(
            [
                '{{ class }}',
                '{{ namespace }}'
            ],
            [
                $name . "ResourceCollection",
                'App\Http\Resources\Collections\\',
            ],
            $this->getStub('resource.collection')
        );

        if(!file_exists($path = app_path('/Http/Resources/Collections')))
            mkdir($path, 0777, true);

        file_put_contents(app_path("/Http/Resources/Collections/{$name}ResourceCollection.php"), $resourceCollectionTemplate);
    }

    protected function seeder($name)
    {
        $seederTemplate = str_replace(
            [
                '{{ class }}',
                '{{ table }}'
            ],
            [
                $name,
                strtolower($names)
            ],
            $this->getStub('seeder')
        );

        file_put_contents(database_path("/seeds/{$name}Seeder.php"), $seederTemplate);
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('FeatureName');
        $modelPlural = $this->argument('FeatureNames');
        $modelVariable = $this->argument('feature_name');
        $modelPluralVariable = $this->argument('feature_names');

        $parentModel = $this->argument('ParentFeatureName');
        $parentModelPlural = $this->argument('ParentFeatureNames');
        $parentModelVariable = $this->argument('parent_feature_name');
        $parentModelPluralVariable = $this->argument('parent_feature_names');
        
        $modelRoute = $modelPluralVariable;
        $namespacedModel = 'App\{$model}';
        $namespacedParentModel = 'App\{$parentModel}';
        $namespacedRequest = 'App\Http\Requests\{$featurename}Request';
        $table = $feature_names;
        
        $this->apiController($featurename, $featurenames);
        $this->blade($featurenames);
        $this->blade_create($featurename, $featurenames);
        $this->blade_edit($featurename, $featurenames);
        $this->blade_index($featurename, $featurenames);
        $this->blade_show($featurename, $featurenames);
        $this->controller($featurename, $featurenames);
        $this->migration($featurename, $featurenames);
        $this->model($featurename, $featurenames);
        $this->request($featurename);
        $this->resource($featurename);
        $this->resource_collection($featurename);
        $this->seeder($featurename, $featurenames);

        File::append(base_path('routes/api.php'), 'Route::apiResource(\'' . $featurenames . "', '{$featurename}APIController');");
        File::append(base_path('routes/web.php'), 'Route::resource(\'' . $featurenames . "', '{$featurename}Controller');");
    }
}
