<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeFeatureCRUD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:crud {FeatureName} {FeatureNames} {feature_name} {feature_names} {columns}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new feature in CRUD';

    protected function getStub($type)
    {
        return file_get_contents("stubs/$type.stub");
    }

    protected function blade($name)
    {
        if(!file_exists($path = resource_path("/views/{strtolower(str_plural($name))}")))
            mkdir($path, 0777, true);
    }
    
    protected function blade_create($name)
    {
        $bladeCreateTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower(str_plural($name))
            ],
            $this->getStub('blade.create')
        );

        file_put_contents(resource_path("/views/{strtolower(str_plural($name))}/create.blade.php"), $bladeCreateTemplate);
    }

    protected function blade_edit($name)
    {
        $bladeEditTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower(str_plural($name))
            ],
            $this->getStub('blade.edit')
        );

        file_put_contents(resource_path("/views/{strtolower(str_plural($name))}/edit.blade.php"), $bladeEditTemplate);
    }

    protected function blade_index($name)
    {
        $bladeIndexTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower(str_plural($name))
            ],
            $this->getStub('blade.index')
        );

        file_put_contents(resource_path("/views/{strtolower(str_plural($name))}/index.blade.php"), $bladeIndexTemplate);
    }

    protected function blade_show($name)
    {
        $bladeShowTemplate = str_replace(
            [
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}'
            ],
            [
                $name,
                strtolower($name),
                strtolower(str_plural($name))
            ],
            $this->getStub('blade.show')
        );

        file_put_contents(resource_path("/views/{strtolower(str_plural($name))}/show.blade.php"), $bladeShowTemplate);
    }

    protected function controller($name)
    {
        $controllerTemplate = str_replace(
            [
                '{{ class }}',
                '{{ model }}',
                '{{ modelVariable }}',
                '{{ modelPluralVariable }}',
                '{{ namespace }}',
                '{{ namespacedModel }}',
                '{{ rootNamespace }}',
            ],
            [
                $name . "Controller",
                $name,
                strtolower($name),
                strtolower(str_plural($name)),
                'App\Http\Controllers',
                'App\\' . $name,
                'App\\'
            ],
            $this->getStub('controller')
        );

        file_put_contents(app_path("/Http/Controllers/{$name}Controller.php"), $controllerTemplate);
    }

    protected function migration($name)
    {
        $migrationTemplate = str_replace(
            [
                '{{ class }}',
                '{{ table }}'
            ],
            [
                'Create' . str_plural($name) . 'Table',
                strtolower(str_plural($name))
            ],
            $this->getStub('migration.create')
        );

        file_put_contents(database_path("/migrations/2020_04_12_0000000_create_{strtolower(str_plural($name))}_table.php"), $migrationTemplate);
    }

    protected function model($name)
    {
        $modelTemplate = str_replace(
            [
                '{{ class }}',
                '{{ namespace }}',
                '{{ table }}'
            ],
            [
                $name,
                'App\\',
                strtolower(str_plural($name))
            ],
            $this->getStub('model')
        );

        file_put_contents(app_path("/{$name}.php"), $modelTemplate);
    }

    protected function request($name)
    {
        $requestTemplate = str_replace(
            [
                '{{ class }}',
                '{{ model }}',
                '{{ modelVariable }}'
            ],
            [
                $name . "Request",
                $name,
                strtolower($name)
            ],
            $this->getStub('request')
        );

        if(!file_exists($path = app_path('/Http/Requests')))
            mkdir($path, 0777, true);

        file_put_contents(app_path("/Http/Requests/{$name}Request.php"), $requestTemplate);
    }

    protected function seeder($name)
    {
        $seederTemplate = str_replace(
            [
                '{{ class }}',
                '{{ table }}'
            ],
            [
                $name,
                strtolower(str_plural($name))
            ],
            $this->getStub('seeder')
        );

        file_put_contents(database_path("/seeds/{$name}Seeder.php"), $seederTemplate);
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // model
        $model = $this->argument('FeatureName');
        // modelVariable
        $modelVariable = $this->argument('feature_name');
        // modelPlural
        $modelPlural = $this->argument('FeatureNames');
        // modelPluralVariable
        $modelPluralVariable = $this->argument('feature_names');
        // modelRoute
        $modelRoute = $feature_names;
        // namespacedModel
        $namespacedModel = 'App\{$featurename}';
        // namespacedRequest
        $namespacedRequest = 'App\Http\Requests\{$featurename}Request';
        // table
        $table = $feature_names;
            
        $this->blade($featurenames);
        $this->blade_create($featurename, $featurenames);
        $this->blade_edit($featurename, $featurenames);
        $this->blade_index($featurename, $featurenames);
        $this->blade_show($featurename, $featurenames);
        $this->controller($featurename, $featurenames);
        $this->migration($featurename, $featurenames);
        $this->model($featurename, $featurenames);
        $this->request($featurename);
        $this->seeder($featurename, $featurenames);

        File::append(base_path('routes/web.php'), 'Route::resource(\'' . $modelRoute . "', '{$featurename}Controller');");
    }
}
