<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lignes de langage de validation
    |--------------------------------------------------------------------------
    |
	| Les lignes de langage suivantes contiennent les messages d’erreur par défaut utilisés par
    | la classe de validation. Certaines de ces règles ont plusieurs versions
    | comme les règles de taille. N’hésitez pas à modifier chacun de ces messages ici.
    |
    */

    'accepted'             => ':attribute doit être accepté.',
    'active_url'           => ":attribute n'est pas une URL valide.",
    'after'                => ':attribute doit être une date postérieure à :date.',
    'after_or_equal'       => ':attribute doit être une date postérieure ou égale à :date.',
    'alpha'                => ':attribute ne peut contenir que des lettres.',
    'alpha_dash'           => ':attribute ne peut contenir que des lettres, des chiffres et des tirets.',
    'alpha_num'            => ':attribute ne peut contenir que des lettres et des chiffres.',
    'array'                => ':attribute doit être un tableau.',
    'before'               => ':attribute doit être une date antérieur à :date.',
    'before_or_equal'      => ':attribute doit être une date antérieur ou égale à :date.',
    'between'              => [
        'numeric' => ':attribute doit être entre :min et :max.',
        'file'    => ':attribute doit être entre :min et :max kilobytes.',
        'string'  => ':attribute doit être entre :min et :max caractères.',
        'array'   => ':attribute doit avoir entre :min et :max éléments.',
    ],
    'boolean'              => 'Le champ :attribute doit être vrai ou faux.',
    'confirmed'            => 'La confirmation :attribute ne correspond pas.',
    'date'                 => ":attribute n'est pas une date valide.",
    'date_format'          => ':attribute ne correspond pas au format :format.',
    'different'            => ':attribute et :other doivent être différent.',
    'digits'               => ':attribute doit être :digits digits.',
    'digits_between'       => ':attribute doit être compris entre :min et :max digits.',
    'dimensions'           => ":attribute a des dimensions d'image non valides.",
    'distinct'             => 'Le champ :attribute a une valeur en double.',
    'email'                => ':attribute doit être une adresse électronique valide.',
    'exists'               => ":attribute sélectionné n'est pas valide.",
    'file'                 => ':attribute doit être un fichier.',
    'filled'               => 'Le champ :attribute doit avoir une valeur.',
    'gt'                   => [
        'numeric' => ':attribute doit être supérieur à :value.',
        'file'    => ':attribute doit être supérieur à :value kilobytes.',
        'string'  => ':attribute doit être supérieur à :value caractères.',
        'array'   => ':attribute doit avoir plus de :value éléments.',
    ],
    'gte'                  => [
        'numeric' => ':attribute doit être supérieur ou égal à :value.',
        'file'    => ':attribute doit être supérieur ou égal à :value kilobytes.',
        'string'  => ':attribute doit être supérieur ou égal à :value caractères.',
        'array'   => ':attribute doit avoir :value éléments ou plus.',
    ],
    'image'                => ':attribute doit être une image.',
    'in'                   => ":attribute sélectionné n'est pas valide.",
    'in_array'             => "Le champ :attribute n'existe pas dans :other.",
    'integer'              => ':attribute doit être un entier.',
    'ip'                   => ':attribute doit être une adresse IP valide.',
    'ipv4'                 => ':attribute doit être une adresse IPv4 valide.',
    'ipv6'                 => ':attribute doit être une adresse IPv6 valide.',
    'json'                 => ':attribute doit être une chaîne de caractères JSON valide.',
    'lt' => [
        'numeric' => ':attribute doit être inférieur à :value.',
        'file' => ':attribute doit être inférieur à :value kilobytes.',
        'string' => ':attribute doit être inférieur à :value caractères.',
        'array' => ':attribute doit avoir moins de :value éléments.',
    ],
    'lte' => [
        'numeric' => ':attribute doit être inférieur ou égal à :value.',
        'file' => ':attribute doit être inférieur ou égal à :value kilobytes.',
        'string' => ':attribute doit être inférieur ou égal à :value caractères.',
        'array' => ':attribute ne doit pas avoir plus de :value éléments.',
    ],
    'max'                  => [
        'numeric' => ':attribute ne peut pas être supérieure à :max.',
        'file'    => ':attribute ne peut pas être supérieure à :max kilobytes.',
        'string'  => ':attribute ne peut pas être supérieure à :max caractères.',
        'array'   => ':attribute ne peut pas avoir plus de :max éléments.',
    ],
    'mimes'                => ':attribute doit être un fichier de type: :values.',
    'mimetypes'            => ':attribute doit être un fichier de type: :values.',
    'min'                  => [
        'numeric' => ':attribute doit être au moins :min.',
        'file'    => ':attribute doit être au moins :min kilobytes.',
        'string'  => ':attribute doit être au moins :min caractères.',
        'array'   => ':attribute doit avoir au moins :min éléments.',
    ],
    'not_in'               => ":attribute sélectionné n'est pas valide.",
    'not_regex'            => "Le format de :attribute n'est pas valide.",
    'numeric'              => ':attribute doit être un nombre.',
    'password'             => 'Le mot de passe est incorrect.',
    'present'              => 'Le champ :attribute doit être présent.',
    'regex'                => 'Le format :attribute est invalide.',
    'required'             => 'Le champ :attribute est requis.',
    'required_if'          => 'Le champ :attribute est requis quand :other est :value.',
    'required_unless'      => 'Le champ :attribute est requis à moins que :other est dans :values.',
    'required_with'        => 'Le champ :attribute est requis quand :values est présent.',
    'required_with_all'    => 'Le champ :attribute est requis quand :values est présent.',
    'required_without'     => "Le champ :attribute est requis quand :values n'est pas présente.",
    'required_without_all' => 'Le champ :attribute est requis quand aucune :values ne sont présentes.',
    'same'                 => 'Le champ :attribute et :other doivent correspondre.',
    'size'                 => [
        'numeric' => ':attribute doit être :size.',
        'file'    => ':attribute doit être :size kilobytes.',
        'string'  => ':attribute doit être :size caractères.',
        'array'   => ':attribute doit contenir :size éléments.',
    ],
    'starts_with'          => ":attribute doit commencer par l'un des éléments suivants : :values.",
    'string'               => ':attribute doit être une chaîne de caractères.',
    'timezone'             => ':attribute doit être une zone valide.',
    'unique'               => ':attribute a déjà été pris.',
    'uploaded'             => "L'upload :attribute a échoué.",
    'url'                  => 'Le format :attribute est invalide.',
    'uuid'                 => ':attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Lignes de langage de validation personnalisée
    |--------------------------------------------------------------------------
    |
    | Ici vous pouvez spécifier des messages de validation personnalisés pour les attributs à l’aide de la
    | convention « attribute.rule » pour nommer les lignes. Cela rend plus rapide à
    | spécifier une ligne de langue personnalisée pour une règle d’attribut donné.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Attributs de validation personnalisée
    |--------------------------------------------------------------------------
    |
    | Les lignes suivantes de la langue sont utilisés pour échanger les attribut-détenteurs de place
    | avec quelque chose de plus agréable à lire telle que l’adresse électronique au lieu
    | des « courriels ». Cela nous aide simplement à faire des messages un peu plus propre.
    |
    */

    'attributes' => [],

];
